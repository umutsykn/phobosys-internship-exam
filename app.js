const fs = require('fs');
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var symbols = [];
var words = [];
rl.question("Please enter the filename (file.txt) :", function(file) {
    fs.readFile(file, 'utf8', function(err, res) {
        if (err) throw err;
        /* optional */
        //res = res.toLowerCase();
        var symbol = res.split("");
        var word = res.split(/[\s,]+/);
        symbol.forEach((symbol) => {
            if(Object.keys(symbols).find(element => element === symbol))
                symbols[symbol]++;
            else
                symbols[symbol] = 1;
        });
        word.forEach((word) => {
            if(Object.keys(words).find(element => element === word))
                words[word]++;
            else
                words[word] = 1;
        });
        var sortSymbols = [];
        for (var e in symbols) {
            sortSymbols.push([e, symbols[e]]);
        }
        sortSymbols.sort(function(a, b) {
            return a[1] - b[1];
        });
        console.log("top 10 most frequent symbols");
        for (i = 0 ; i < 11 ;i++){
            console.log(sortSymbols[sortSymbols.length-i]);
        }
        var sortWords = [];
        for (var e in words) {
            sortWords.push([e, words[e]]);
        }
        sortWords.sort(function(a, b) {
            return a[1] - b[1];
        });
        console.log("top 20 most frequent words");
        for (i = 0 ; i < 21 ;i++){
            console.log(sortWords[sortWords.length-i]);
        }
    });
});

